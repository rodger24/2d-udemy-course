﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
	
	public float moveSpeed;
	public Rigidbody2D rb;
	public float jumpForce;
	
	private bool isGrounded;
	public Transform groundCheckPoint;
	public LayerMask whatIsGround;
	private bool canDoubleJump;
	private SpriteRenderer sr;
	
	private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
		isGrounded = true;
		///Input.GetAxis("Horizontal")
        rb.velocity = new Vector2(moveSpeed * Input.GetAxisRaw("Horizontal"),rb.velocity.y);
		
		///isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position,.01f,whatIsGround);
		
		/*if (isGrouned) {
				canDoubleJump = true; 
		}*/
		
        if (Input.GetButtonDown("Jump")) 
        {
			//if (isGrounded) {
				rb.velocity = new Vector2(rb.velocity.x, jumpForce);
				//isGrounded = false;
				//canDoubleJump = true; 
		//	/}
		/*	else 
			{
				if (canDoubleJump) {
					rb.velocity = new Vector2(rb.velocity.x, jumpForce);
					canDoubleJump = false;

				}
			}*/
			
		}
		
		if (rb.velocity.x < 0) {
			sr.flipX = true;
		}
		
		else if (rb.velocity.x > 0) {
			sr.flipX = false;
		}
		///anim.SetBool("isGrounded",isGrounded);
		anim.SetFloat("moveSpeed",Mathf.Abs(rb.velocity.x));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform toFollow; 
	
	public Transform farBackground; 
	public Transform middleBackground;
	public float minHeight;
	public float maxHeight;
	
	
	//private float lastXPos;
	private Vector2 lastPos;
	
    // Start is called before the first frame update
    void Start()
    {
        lastPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
		float clampedY = Mathf.Clamp(toFollow.position.y, minHeight, maxHeight);
	
        transform.position = new Vector3(toFollow.position.x, clampedY ,transform.position.z);
	
		Vector2 amountToMove = new Vector2(transform.position.x - lastPos.x, transform.position.y - lastPos.y); 
		///float amountToMoveX = transform.position.x - lastXPos;
		
		farBackground.position +=  new Vector3(amountToMove.x, amountToMove.y, 0f);
		middleBackground.position += new Vector3(amountToMove.x , amountToMove.y, 0f) * .5f;
		lastPos = transform.position;

    }
}

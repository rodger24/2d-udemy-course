﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIController : MonoBehaviour
{
	public static UIController instance;
    // Start is called before the first frame update

	public Image heart1, heart2, heart3;
	public Sprite heartFull, heartEmpty, heartHalf;
	

    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void UpdateHealthDisplay() {
		switch (PlayerHealthController.instance.health) {
			case 6:
				heart1.sprite = heartFull;
				heart2.sprite = heartFull;
				heart3.sprite = heartFull;
				break;
				
			case 5:
				heart3.sprite = heartHalf;
				heart2.sprite = heartFull;
				heart1.sprite = heartFull;
				break; 	
			case 4:
				heart3.sprite = heartEmpty;
				heart2.sprite = heartFull;
				heart1.sprite = heartFull;
				break;
			
			case 3:
				heart3.sprite = heartEmpty;
				heart2.sprite = heartHalf;
				heart1.sprite = heartFull;
				break;	
			case 2:
				heart3.sprite = heartEmpty;
				heart2.sprite = heartEmpty;
				heart1.sprite = heartFull;
				break;
			case 1: 
				heart3.sprite = heartEmpty;
				heart2.sprite = heartEmpty;
				heart1.sprite = heartHalf;
				break;
			
			case 0: 
				heart3.sprite = heartEmpty;
				heart2.sprite = heartEmpty;
				heart1.sprite = heartEmpty;
				break;
				
			default:
				break;
		}
	}
}
